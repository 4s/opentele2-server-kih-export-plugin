class KihExportGrailsPlugin {
    // the plugin version
    def version = "2.12.0"
    // the version or versions of Grails the plugin is designed for
    def grailsVersion = "2.4 > *"
    // resources that are excluded from plugin packaging
    def pluginExcludes = [
            "grails-app/views/error.gsp"
    ]

    def title = "OpenTele Kih Export Plugin"
    def author = "OpenTeleHealth"
    def authorEmail = ""
    def description = "OpenTele kih export plugin contains functionality for sending measurements to the KIH database."

}
