package org.opentele.server

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import oio.medcom.chronicdataset._1_0.InstrumentType
import oio.medcom.chronicdataset._1_0.LaboratoryReportExtendedType
import oio.medcom.chronicdataset._1_0.MeasurementLocationType
import oio.medcom.chronicdataset._1_0.MeasurementScheduledType
import oio.medcom.chronicdataset._1_0.MeasurementTransferredByType
import oio.medcom.chronicdataset._1_0.ObjectFactory
import org.opentele.builders.MeasurementBuilder
import org.opentele.builders.MeasurementTypeBuilder
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.kih_device_export_types.Nonin9560ExportType
import org.opentele.server.kih_measurement_export_types.BloodPressureDiastolicExportType
import org.opentele.server.kih_measurement_export_types.SaturationExportType
import org.opentele.server.kih_measurement_export_types.TemperatureExportType
import org.opentele.server.model.DeviceOrigin
import org.opentele.server.model.ManualInputOrigin
import org.opentele.server.model.Measurement
import org.opentele.server.model.MeasurementType
import org.opentele.server.model.Patient
import org.opentele.server.model.patientquestionnaire.CompletedQuestionnaire
import spock.lang.Specification

@TestFor(KihDbWebService)
@Build([Patient, Measurement, MeasurementType, ManualInputOrigin, DeviceOrigin, CompletedQuestionnaire])
class KihDbWebServiceSpec extends Specification {

    def patient
    def completedQuestionnaire

    def bloodPressureMeasurementType
    def saturationMeasurementType

    def setup() {
        patient = Patient.build()
        completedQuestionnaire = new CompletedQuestionnaire(patient: patient)
        bloodPressureMeasurementType = new MeasurementTypeBuilder()
                .ofType(MeasurementTypeName.BLOOD_PRESSURE).build()
        saturationMeasurementType = new MeasurementTypeBuilder()
                .ofType(MeasurementTypeName.SATURATION).build()
    }

    def "should add generic info on valid input"() {
        given:
        def laboratoryReport = new LaboratoryReportExtendedType()
        def time = new Date()
        def analysisText = "Blodtryk systolisk; arm"
        def value = 120
        def unitText = "slag i minuttet"
        def npuCode = NpuCode.BLOOD_PRESSURE_SYSTOLIC

        when:
        service.addGenericInfo(laboratoryReport, time, analysisText, value, unitText, npuCode)

        then:
        laboratoryReport.uuidIdentifier != null
        laboratoryReport.createdDateTime != null
        laboratoryReport.analysisText == analysisText
        laboratoryReport.resultText == value as String
        laboratoryReport.resultUnitText == unitText
        laboratoryReport.iupacIdentifier == npuCode.value()
        laboratoryReport.nationalSampleIdentifier == "9999999999"
        laboratoryReport.resultTypeOfInterval == "unspecified"

    }

    def "should add producer info on valid input"() {
        given:
        def objectFactory = new ObjectFactory()
        def laboratoryReport = new LaboratoryReportExtendedType()

        when:
        service.addProducerInfo(objectFactory, laboratoryReport)

        then:
        laboratoryReport.producerOfLabResult != null
        laboratoryReport.producerOfLabResult.identifier == 'Patient målt'
        laboratoryReport.producerOfLabResult.identifierCode == 'POT'
    }

    def "should add instrument info on valid input"() {
        given:
        def measurement = new MeasurementBuilder()
                .ofType(MeasurementTypeName.BLOOD_PRESSURE)
                .inQuestionnaire(completedQuestionnaire)
                .build()
        def laboratoryReport = new LaboratoryReportExtendedType()
        def objectFactory = new ObjectFactory()
        def exportType = new TemperatureExportType()

        when:
        service.addInstrumentInfo(laboratoryReport, objectFactory, measurement, exportType)

        then:
        laboratoryReport.instrument != null
        laboratoryReport.measurementLocation == MeasurementLocationType.HOME
        laboratoryReport.measurementScheduled == MeasurementScheduledType.SCHEDULED
    }

    def "should add device measurement info on valid input"() {
        given:
        def laboratoryReport = new LaboratoryReportExtendedType()
        laboratoryReport.instrument = new InstrumentType()
        def origin = new DeviceOrigin()
        origin.manufacturer = "A&D Medical"
        origin.model = "UA-767PlusBT-Ci Bluetooth"
        origin.softwareVersion = "1.0.4"
        def exportType = new BloodPressureDiastolicExportType()

        when:
        service.addDeviceMeasurementInfo(laboratoryReport, origin, exportType)

        then:
        laboratoryReport.measurementTransferredBy == MeasurementTransferredByType.AUTOMATIC
        laboratoryReport.instrument.manufacturer == origin.manufacturer
        laboratoryReport.instrument.model == origin.model
        laboratoryReport.instrument.softwareVersion == origin.softwareVersion
        laboratoryReport.instrument.medComID == "MCI00012"
        laboratoryReport.instrument.productType != null
    }

    @SuppressWarnings("GroovyAccessibility")
    def "should return null when looking up with invalid measurement export type"() {
        given:
        def exportType = new SaturationExportType()
        def deviceExportType = new Nonin9560ExportType()

        def origin = DeviceOrigin.build()
        origin.manufacturer = 'WRONG!'
        exportType.knownDevices = [deviceExportType]

        when:
        def result = service.lookupDeviceExportType(exportType, origin)

        then:
        result == null
    }

    @SuppressWarnings("GroovyAccessibility")
    def "should return device export type when looking up with valid measurement export type"() {
        given:
        def exportType = new SaturationExportType()
        def deviceExportType = new Nonin9560ExportType()

        def origin = DeviceOrigin.build()
        origin.manufacturer = 'Nonin'
        origin.model = 'Onyx II 9560 Nonin'
        exportType.knownDevices = [deviceExportType]

        when:
        def result = service.lookupDeviceExportType(exportType, origin)

        then:
        result != null
        result == deviceExportType
    }

    def "should set measurementTransferredBy to typed when adding default measurement info"() {
        given:
        def origin = ManualInputOrigin.build()
        def laboratoryReport = new LaboratoryReportExtendedType()

        when:
        service.addManualInputMeasurementInfo(origin, laboratoryReport)

        then:
        laboratoryReport.measurementTransferredBy == MeasurementTransferredByType.TYPED
    }

    def "should set measurementTransferredBy to clinician typed when adding default measurement info"() {
        given:
        def origin = ManualInputOrigin.build()
        origin.enteredBy = 'clinician'
        def laboratoryReport = new LaboratoryReportExtendedType()

        when:
        service.addManualInputMeasurementInfo(origin, laboratoryReport)

        then:
        laboratoryReport.measurementTransferredBy == MeasurementTransferredByType.TYPEDBYHCPROF
    }


}