package org.opentele.server

import grails.buildtestdata.mixin.Build
import grails.test.mixin.TestFor
import org.opentele.server.model.Measurement
import org.opentele.server.model.MeasurementType
import org.opentele.server.core.model.types.GlucoseInUrineValue
import org.opentele.server.core.model.types.MeasurementTypeName
import spock.lang.Specification

@TestFor(KihDbExportService)
@Build([Measurement, MeasurementType])
class KihDbExportServiceSpec extends Specification {

    def kihDbExportService
    def kihDbWebService
    def sosiService

    def setup() {
        kihDbExportService = new KihDbExportService()

        kihDbWebService = [
                sendMeasurement: { measurement, request, kihUrl,  createdByText -> }
        ]
        service.kihDbWebService = kihDbWebService

        sosiService = [
                createRequest: { -> null }
        ]
        service.sosiService = sosiService
    }

    def "should not try to export already exported measurements of the specified types"() {
        setup:
        Map<String, Map> values = [
                "CTG": [fhr: "fhr"],
                "HEMOGLOBIN": [value: 0],
                "TEMPERATURE": [value: 0],
                "URINE_GLUCOSE": [glucoseInUrine: GlucoseInUrineValue.NEGATIVE]
        ]

        def notToBeExported = Measurement.notToBeExportedMeasurementTypes.collect { MeasurementTypeName typeName ->
            def type = MeasurementType.build(name: typeName)
            Map value = [exportedToKih: false, measurementType: type]
            def entry = values.get(typeName.name())
            if (entry) {
                value += entry
            }
            Measurement.build(value)
        }

        def bloodSugarType = MeasurementType.build(name: MeasurementTypeName.BLOODSUGAR)
        def bloodSugar = Measurement.build(exportedToKih: false, measurementType: bloodSugarType, value: 0)

        def alreadyExported = Measurement.build(exportedToKih: true, measurementType: bloodSugarType, value: 0)
        notToBeExported << alreadyExported

        when:
        def exported = service.getMeasurementsToExport()

        then:
        notToBeExported.size() >= 2
        exported.contains(bloodSugar)
        notToBeExported.every { !exported.contains(it) }
    }

    def "should mark new measurements as exported if export succeeds"() {
        setup:
        def type = MeasurementType.build(name: MeasurementTypeName.BLOOD_PRESSURE)
        Map value = [exportedToKih: false, measurementType: type, systolic: 120,
                     diastolic: 80, meanArterialPressure: 95]
        Measurement measurement = Measurement.build(value)
        String kihUrl = "fake_url"
        String createdByText = "OpenTele"

        when:
        service.exportToKihDatabase(kihUrl, createdByText)

        then:
        measurement.exportedToKih
    }

    def "should not mark measurements as exported if export fails"() {
        setup:
        def type = MeasurementType.build(name: MeasurementTypeName.BLOOD_PRESSURE)
        Map value = [exportedToKih: false, measurementType: type, systolic: 120,
                     diastolic: 80, meanArterialPressure: 95]
        Measurement measurement = Measurement.build(value)

        service.kihDbWebService = [
                sendMeasurement: { m, r -> throw new Exception("Something went wrong") }
        ]

        String kihUrl = "fake_url"
        String createdByText = "OpenTele"

        when:
        service.exportToKihDatabase(kihUrl, createdByText)

        then:
        !measurement.exportedToKih
    }

}
