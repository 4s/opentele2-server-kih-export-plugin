grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

// Repository to push plugin to
grails.project.repos.oth.url = "https://repository.oth.io/nexus/content/repositories/opentele-server-plugins/"
grails.project.repos.oth.username = "oth-developer"
grails.project.repos.oth.password = "<insert-password>"
grails.project.repos.default = "oth"

grails.project.dependency.resolution = {

    inherits("global") { }

    log "warn" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'

    repositories {
        grailsCentral()
        mavenCentral()
        mavenRepo "https://repository.oth.io/nexus/content/repositories/build-dependencies/"
    }

    dependencies {
        runtime 'com.github.groovy-wslite:groovy-wslite:0.7.2'
        runtime('dk.sosi.seal:seal:2.1.3') {
            exclude "axis-jaxrpc"
        }
        // DGKS - version 1.0.2
        compile('dk.medcom:dgks-types:0.5')
    }

    plugins {
        compile ':spring-security-core:2.0-RC4'
        build(":tomcat:7.0.54",
                ":release:3.0.1",
                ":rest-client-builder:2.0.3") {
            export = false
        }
    }
}

def corePluginDirectory = '../opentele-server-core-plugin'
grails.plugin.location.'OpenteleServerCorePlugin' = corePluginDirectory

