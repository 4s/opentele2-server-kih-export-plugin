package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.AnDMedical302PBTExportType
import org.opentele.server.kih_device_export_types.DeviceExportType

import java.text.DecimalFormat

class TemperatureExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.TEMPERATURE
    private static String longName = "Pt—Legeme; temp. = ? °C"
    private static String shortName = "Legeme temp.;Pt"
    private static String unit = "°C"
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 37.6
        final String EXACTLY_ONE_DECIMAL_PLACE = "0.0"
        DecimalFormat df = new DecimalFormat(EXACTLY_ONE_DECIMAL_PLACE)
        df.format(measurement.getValue()).replace(',','.')
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = [
            new AnDMedical302PBTExportType()
    ]

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }


    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
