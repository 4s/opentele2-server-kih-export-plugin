package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

import java.text.DecimalFormat

class CrpExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.CRP
    private static String longName = "P—C-reaktivt protein; massek. = ? mg/L"
    private static String shortName = "C-reaktivt protein [CRP];P"
    private static String unit = "mg/L"
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 28
        final String NO_DECIMAL_PLACES = "0"
        DecimalFormat df = new DecimalFormat(NO_DECIMAL_PLACES)
        df.format(measurement.getValue())
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = []

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
