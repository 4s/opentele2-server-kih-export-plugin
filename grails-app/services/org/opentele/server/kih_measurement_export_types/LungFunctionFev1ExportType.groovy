package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType
import org.opentele.server.kih_device_export_types.Vitalograph4000ExportType

import java.text.DecimalFormat

class LungFunctionFev1ExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.FEV1
    private static String longName = "Lunge—Lungefunktionsundersøgelse FEV1; vol. = ? L"
    private static String shortName = "FEV1;Lunge"
    private static String unit = "L"
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 4.01
        final String EXACTLY_TWO_DECIMAL_PLACES = "0.00"
        DecimalFormat df = new DecimalFormat(EXACTLY_TWO_DECIMAL_PLACES)
        df.format(measurement.getValue()).replace(',','.')
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = [
            new Vitalograph4000ExportType()
    ]

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
