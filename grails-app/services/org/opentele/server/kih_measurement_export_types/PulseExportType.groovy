package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.AnDMedical767PBTCExportType
import org.opentele.server.kih_device_export_types.DeviceExportType
import org.opentele.server.kih_device_export_types.Nonin3230ExportType
import org.opentele.server.kih_device_export_types.Nonin9560ExportType

import java.text.DecimalFormat

class PulseExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.PULSE
    private static String longName = "Hjerte—Systole; frekv. = ? × 1/min"
    private static String shortName = "Puls;Hjerte"
    private static String unit = "× 1/min"
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 65
        final String NO_DECIMAL_PLACES = "0"
        DecimalFormat df = new DecimalFormat(NO_DECIMAL_PLACES)
        df.format(measurement.getValue())
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = [
            new Nonin3230ExportType(),
            new Nonin9560ExportType(),
            new AnDMedical767PBTCExportType(),
            new AnDMedical767PBTCExportType()
    ]

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
