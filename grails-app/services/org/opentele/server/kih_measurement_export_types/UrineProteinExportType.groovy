package org.opentele.server.kih_measurement_export_types

import org.opentele.server.core.model.types.ProteinValue
import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

class UrineProteinExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.URINE_PROTEIN
    private static String longName = "U—Protein; arb.k.(proc.) = ?"
    private static String shortName = "Protein(semikvant);U"
    private static String unit = ""
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 0, value set: {0,1,2,3}
        String value = null;
        switch (measurement.getProtein()) {
            case ProteinValue.NEGATIVE:
                value = "0"
                break
            case ProteinValue.PLUSMINUS:
                value = "0"
                break
            case ProteinValue.PLUS_ONE:
                value = "1"
                break
            case ProteinValue.PLUS_TWO:
                value = "2"
                break
            case ProteinValue.PLUS_THREE:
                value = "3"
                break
            case ProteinValue.PLUS_FOUR:
                value = "3"
                break
        }
        value
    }
    private static boolean isAlphanumeric = true
    private static List<DeviceExportType> knownDevices = []

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
