package org.opentele.server.kih_measurement_export_types

import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

interface MeasurementExportType {

    NpuCode getAnalysisCode()
    String getLongName()
    String getShortName()
    String getUnit()
    Closure<String> getValueClosure()
    Closure getCustomClosure()
    boolean isAlphanumeric()
    List<DeviceExportType> getKnownDevices()

}
