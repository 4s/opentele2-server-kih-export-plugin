package org.opentele.server.kih_measurement_export_types

import org.opentele.server.core.model.types.NitriteInUrineValue
import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

class UrineNitriteExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.URINE_NITRITE
    private static String longName = "U—Nitrit; arb.k.(proc.) = ?"
    private static String shortName = "Nitrit (semikvant);U"
    private static String unit = ""
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 1, value set: {0,1}
        String value = null;
        switch (measurement.getNitriteInUrine()) {
            case NitriteInUrineValue.NEGATIVE:
                value = "0"
                break
            case NitriteInUrineValue.POSITIVE:
                value = "1"
                break
        }
        value
    }
    private static boolean isAlphanumeric = true
    private static List<DeviceExportType> knownDevices = []

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
