package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

import java.text.DecimalFormat

class BloodSugarExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.BLOOD_SUGAR
    private static String longName = "P(kB)—Glucose; stofk. = ? mmol/L"
    private static String shortName = "Glukose;P(kB)"
    private static String unit = "mmol/l"

    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 8.8
        final String EXACTLY_ONE_DECIMAL_PLACE = "0.0"
        DecimalFormat df = new DecimalFormat(EXACTLY_ONE_DECIMAL_PLACE)
        df.format(measurement.getValue()).replace(',','.')
    }

    private static Closure customClosure = { def type, Measurement measurement ->
        def circumstances

        if (measurement.isAfterMeal) {
            circumstances = 'Efter måltid'
        } else if (measurement.isBeforeMeal) {
            circumstances =  'Før måltid'
        } else if (measurement.isControlMeasurement) {
            circumstances =  'Kontrolmåling'
        }

        if (circumstances) {
            type.measuringCircumstances = circumstances
        }
    }

    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = []

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        customClosure
    }

}
