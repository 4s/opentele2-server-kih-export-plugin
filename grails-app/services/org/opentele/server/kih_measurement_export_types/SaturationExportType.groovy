package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType
import org.opentele.server.kih_device_export_types.Nonin3230ExportType
import org.opentele.server.kih_device_export_types.Nonin9560ExportType

import java.text.DecimalFormat

class SaturationExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.SATURATION
    private static String longName = "Hb(Fe; O2-bind.; aB)—Oxygen(O2); mætn. = ?"
    private static String shortName = "O2 sat.;Hb(aB)"
    private static String unit = ""
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 0,98
        final String EXACTLY_TWO_DECIMAL_PLACES = "0.00"
        DecimalFormat df = new DecimalFormat(EXACTLY_TWO_DECIMAL_PLACES)
        double exportValue = measurement.getValue() / 100 // OpenTele stores: 97 instead of 0,97
        df.format(exportValue).replace(',','.')
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = [
            new Nonin9560ExportType(),
            new Nonin3230ExportType()
    ]

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
