package org.opentele.server.kih_measurement_export_types

import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.AnDMedical767PBTCExportType
import org.opentele.server.kih_device_export_types.AnDMedical767PBTCiExportType
import org.opentele.server.kih_device_export_types.DeviceExportType

import java.text.DecimalFormat

class BloodPressureDiastolicExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.BLOOD_PRESSURE_DIASTOLIC
    private static String longName = "Arm—Blodtryk(diastolisk); tryk = ? mmHg"
    private static String shortName = "Blodtryk diastolisk;Arm"
    private static String unit = "mmHg"
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 130
        final String NO_DECIMAL_PLACES = "0"
        DecimalFormat df = new DecimalFormat(NO_DECIMAL_PLACES)
        df.format(measurement.getDiastolic())
    }
    private static boolean isAlphanumeric = false
    private static List<DeviceExportType> knownDevices = [
            new AnDMedical767PBTCiExportType(),
            new AnDMedical767PBTCExportType()
    ]

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
