package org.opentele.server.kih_measurement_export_types

import org.opentele.server.core.model.types.LeukocytesInUrineValue
import org.opentele.server.model.Measurement
import org.opentele.server.NpuCode
import org.opentele.server.kih_device_export_types.DeviceExportType

class UrineLeukocytesExportType implements MeasurementExportType {

    private static NpuCode analysisCode = NpuCode.URINE_LEUKOCYTES
    private static String longName = "U—Leukocytter; arb.k.(proc.) = ?"
    private static String shortName = "Leukocytter(semikvant);U"
    private static String unit = ""
    private static Closure<String> valueClosure = { Measurement measurement -> // example value: 2, value set: {0,1,2,3}
        String value = null;
        switch (measurement.getLeukocytesInUrine()) {
            case LeukocytesInUrineValue.NEGATIVE:
                value = "0"
                break
            case LeukocytesInUrineValue.PLUS_ONE:
                value = "1"
                break
            case LeukocytesInUrineValue.PLUS_TWO:
                value = "2"
                break
            case LeukocytesInUrineValue.PLUS_THREE:
                value = "3"
                break
            case LeukocytesInUrineValue.PLUS_FOUR:
                value = "3"
                break
        }
        value
    }
    private static boolean isAlphanumeric = true
    private static List<DeviceExportType> knownDevices = []

    @Override
    NpuCode getAnalysisCode() {
        analysisCode
    }

    @Override
    String getLongName() {
        longName
    }

    @Override
    String getShortName() {
        shortName
    }

    @Override
    String getUnit() {
        unit
    }

    @Override
    Closure getValueClosure() {
        valueClosure
    }

    @Override
    boolean isAlphanumeric() {
        isAlphanumeric
    }

    @Override
    List<DeviceExportType> getKnownDevices() {
        knownDevices
    }

    @Override
    Closure getCustomClosure() {
        return null
    }

}
