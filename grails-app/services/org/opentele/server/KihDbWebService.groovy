package org.opentele.server

import dk.sosi.seal.model.Request
import oio.medcom.chronicdataset._1_0.CitizenType
import oio.medcom.chronicdataset._1_0.EncodingIdentifierType
import oio.medcom.chronicdataset._1_0.LaboratoryReportExtendedType
import oio.medcom.chronicdataset._1_0.MeasurementLocationType
import oio.medcom.chronicdataset._1_0.MeasurementScheduledType
import oio.medcom.chronicdataset._1_0.MeasurementTransferredByType
import oio.medcom.chronicdataset._1_0.ObjectFactory
import oio.medcom.chronicdataset._1_0.SelfMonitoredSampleType
import oio.medcom.monitoringdataset._1_0.CreateMonitoringDatasetRequestMessage
import oio.medcom.monitoringdataset._1_0.MonitoringDatasetCollectionType
import org.opentele.server.core.model.types.MeasurementTypeName
import org.opentele.server.model.DeviceOrigin
import org.opentele.server.model.ManualInputOrigin
import org.opentele.server.model.Measurement
import org.opentele.server.model.Origin
import org.opentele.server.kih_device_export_types.DeviceExportType
import org.opentele.server.kih_measurement_export_types.BloodPressureDiastolicExportType
import org.opentele.server.kih_measurement_export_types.BloodPressureSystolicExportType
import org.opentele.server.kih_measurement_export_types.BloodSugarExportType
import org.opentele.server.kih_measurement_export_types.CrpExportType
import org.opentele.server.kih_measurement_export_types.LungFunctionFev1ExportType
import org.opentele.server.kih_measurement_export_types.MeasurementExportType
import org.opentele.server.kih_measurement_export_types.PulseExportType
import org.opentele.server.kih_measurement_export_types.SaturationExportType
import org.opentele.server.kih_measurement_export_types.TemperatureExportType
import org.opentele.server.kih_measurement_export_types.UrineBloodExportType
import org.opentele.server.kih_measurement_export_types.UrineGlucoseExportType
import org.opentele.server.kih_measurement_export_types.UrineLeukocytesExportType
import org.opentele.server.kih_measurement_export_types.UrineNitriteExportType
import org.opentele.server.kih_measurement_export_types.UrineProteinExportType
import org.opentele.server.kih_measurement_export_types.WeightExportType
import wslite.soap.SOAPClient
import wslite.soap.SOAPResponse

import javax.xml.datatype.DatatypeFactory

class KihDbWebService {

    private static final String MEASURED_BY_PATIENT = "Patient målt"
    private static final String POT_CODE = "POT"
    private static final String NATIONAL_SAMPLE_IDENTIFIER = "9999999999"
    private static final String UNSPECIFIED = "unspecified"
    private static final String CLINICIAN = 'clinician'

    private static final Map<String, List<MeasurementExportType>> measurementTypeNameToExportTypesMap = [:]

    void sendMeasurement(Measurement measurement, Request request, String kihUrl, String createdByText) {
        CreateMonitoringDatasetRequestMessage requestMessage = wrapMeasurementInRequestMessage(measurement, createdByText)
        List result = send(requestMessage, request, kihUrl)
        log.info("\nResult from KihDB: ${result}")
        result
    }

    static List send(CreateMonitoringDatasetRequestMessage datasetRequestMessage, Request request, String kihUrl) {
        String soapRequest = RequestUtil.generateRequest(datasetRequestMessage, request)
        def soapClient = new SOAPClient(kihUrl)
        SOAPResponse response = soapClient.send(soapRequest)
        response.CreateMonitoringDatasetResponseMessage.MonitoringDatasetCollectionResponse.UuidIdentifier.list()
    }

    static CreateMonitoringDatasetRequestMessage wrapMeasurementInRequestMessage(Measurement measurement, String createdByText) {
        CreateMonitoringDatasetRequestMessage requestMessage = new CreateMonitoringDatasetRequestMessage()
        List<MonitoringDatasetCollectionType> monitoringDatasets = requestMessage.getMonitoringDatasetCollection()
        MonitoringDatasetCollectionType monitoringDataSet = createMonitoringDataset(measurement, createdByText)
        monitoringDatasets.add(monitoringDataSet)
        requestMessage
    }

    static MonitoringDatasetCollectionType createMonitoringDataset(Measurement measurement, String createdByText) {
        MonitoringDatasetCollectionType dataset = new MonitoringDatasetCollectionType()

        dataset.citizen = createCitizenType(measurement)

        ObjectFactory objectFactory = new ObjectFactory()
        List<SelfMonitoredSampleType> selfMonitoredSampleTypes =
                wrapMeasurementInLaboratoryReport(objectFactory, measurement, createdByText)
        dataset.selfMonitoredSample.addAll(selfMonitoredSampleTypes)

        dataset
    }

    private static CitizenType createCitizenType(Measurement measurement) {
        CitizenType citizenType = new CitizenType()
        citizenType.personCivilRegistrationIdentifier = measurement.getPatient().getCpr()
        citizenType
    }

    static List<SelfMonitoredSampleType> wrapMeasurementInLaboratoryReport(ObjectFactory objectFactory,
                                                                           Measurement measurement,
                                                                           String createdByText) {
        List<SelfMonitoredSampleType> selfMonitoredSampleTypes = new ArrayList<SelfMonitoredSampleType>()

        SelfMonitoredSampleType selfMonitoredSampleType = objectFactory.createSelfMonitoredSampleType()
        selfMonitoredSampleType.createdByText = createdByText
        selfMonitoredSampleType.laboratoryReportExtendedCollection =
                objectFactory.createLaboratoryReportExtendedCollectionType()
        addMeasurementToLaboratoryReport(measurement, selfMonitoredSampleType, objectFactory)

        selfMonitoredSampleTypes.add(selfMonitoredSampleType)
        selfMonitoredSampleTypes
    }

    static void addMeasurementToLaboratoryReport(Measurement measurement,
                                                 SelfMonitoredSampleType selfMonitoredSampleType,
                                                 ObjectFactory objectFactory) {
        if (measurementTypeNameToExportTypesMap.isEmpty()) {
            fillMeasurementTypeNameToClosureMap()
        }

        String measurementTypeName = measurement.measurementType.name
        List<MeasurementExportType> measurementExportTypes = measurementTypeNameToExportTypesMap[measurementTypeName]
        if (measurementExportTypes) {
            handleMeasurementType(measurement, selfMonitoredSampleType, objectFactory, measurementExportTypes)
        } else {
            throw new RuntimeException("Unsupported measurement type ${measurement.getMeasurementType().getName()} encountered.")
        }
    }

    static void handleMeasurementType(Measurement measurement,
                                      SelfMonitoredSampleType selfMonitoredSampleType,
                                      ObjectFactory objectFactory,
                                      List<MeasurementExportType> measurementExportTypes) {
        measurementExportTypes.each { MeasurementExportType measurementExportType ->

            def value = measurementExportType.getValueClosure() << measurement
            String unitText = measurementExportType.getUnit()
            NpuCode analysisCode = measurementExportType.getAnalysisCode()
            String analysisText = measurementExportType.getLongName()
            boolean isAlphanumeric = measurementExportType.isAlphanumeric()

            def type = createExtendedType(value, unitText, measurement.getTime(), analysisCode, analysisText,
                    objectFactory, measurement, measurementExportType)

            Closure customClosure = measurementExportType.getCustomClosure()
            if (customClosure) {
                customClosure(type, measurement)
            }

            if (isAlphanumeric) {
                type.resultEncodingIdentifier = EncodingIdentifierType.ALPHANUMERIC
            } else {
                type.resultEncodingIdentifier = EncodingIdentifierType.NUMERIC
            }

            selfMonitoredSampleType.laboratoryReportExtendedCollection.getLaboratoryReportExtended().add(type)
            measurement.setKihUuid(type.getUuidIdentifier())
        }
    }

    static LaboratoryReportExtendedType createExtendedType(def value, String unitText, Date time,
                                                           NpuCode analysisCode, String analysisText,
                                                           ObjectFactory objectFactory, Measurement measurement,
                                                           MeasurementExportType measurementExportType) {

        LaboratoryReportExtendedType laboratoryReportExtendedType = new LaboratoryReportExtendedType()
        addGenericInfo(laboratoryReportExtendedType, time, analysisText, value, unitText, analysisCode)
        addProducerInfo(objectFactory, laboratoryReportExtendedType)
        addInstrumentInfo(laboratoryReportExtendedType, objectFactory, measurement, measurementExportType)

        laboratoryReportExtendedType
    }

    static void addGenericInfo(LaboratoryReportExtendedType laboratoryReportExtendedType, Date time,
                               String analysisText, def value, String unitText, NpuCode analysisCode) {
        laboratoryReportExtendedType.uuidIdentifier = UUID.randomUUID().toString()
        laboratoryReportExtendedType.createdDateTime = getDateAsXml(time)
        laboratoryReportExtendedType.analysisText = analysisText
        laboratoryReportExtendedType.resultText = value
        laboratoryReportExtendedType.resultUnitText = unitText
        laboratoryReportExtendedType.iupacIdentifier = analysisCode?.value()
        laboratoryReportExtendedType.nationalSampleIdentifier = NATIONAL_SAMPLE_IDENTIFIER
        laboratoryReportExtendedType.resultTypeOfInterval = UNSPECIFIED
    }

    static void addProducerInfo(ObjectFactory objectFactory, LaboratoryReportExtendedType laboratoryReportExtendedType) {
        laboratoryReportExtendedType.producerOfLabResult = objectFactory.createProducerOfLabResultType()
        laboratoryReportExtendedType.producerOfLabResult.identifier = MEASURED_BY_PATIENT
        laboratoryReportExtendedType.producerOfLabResult.identifierCode = POT_CODE
    }

    static void addInstrumentInfo(LaboratoryReportExtendedType laboratoryReportExtendedType,
                                  ObjectFactory objectFactory, Measurement measurement,
                                  MeasurementExportType measurementExportType) {

        laboratoryReportExtendedType.instrument = objectFactory.createInstrumentType()
        Origin origin = measurement.getOrigin()

        if (origin instanceof DeviceOrigin) {
            addDeviceMeasurementInfo(laboratoryReportExtendedType, origin, measurementExportType)
        } else if (origin instanceof ManualInputOrigin) {
            addManualInputMeasurementInfo(origin, laboratoryReportExtendedType)
        } else {
            addDefaultMeasurementInfo(laboratoryReportExtendedType)
        }

        laboratoryReportExtendedType.measurementLocation = MeasurementLocationType.HOME
        laboratoryReportExtendedType.measurementScheduled = MeasurementScheduledType.SCHEDULED
    }

    static void addDeviceMeasurementInfo(LaboratoryReportExtendedType laboratoryReportExtendedType,
                                         DeviceOrigin origin, MeasurementExportType measurementExportType) {
        laboratoryReportExtendedType.measurementTransferredBy = MeasurementTransferredByType.AUTOMATIC

        DeviceOrigin deviceOrigin = (DeviceOrigin) origin
        DeviceExportType deviceExportType = lookupDeviceExportType(measurementExportType, deviceOrigin)
        if (deviceExportType) {
            def instrument = laboratoryReportExtendedType.instrument
            instrument.medComID = deviceExportType.getMedComId()
            instrument.productType = deviceExportType.getProductType()
            instrument.manufacturer = deviceExportType.getManufacturer()
            instrument.model = deviceExportType.getModel()
            instrument.softwareVersion = deviceOrigin.softwareVersion
        }
    }

    static DeviceExportType lookupDeviceExportType(MeasurementExportType exportType, DeviceOrigin deviceOrigin) {
        DeviceExportType deviceExportType = exportType.knownDevices.find { DeviceExportType deviceExportType ->
            deviceExportType.checkIfSameDevice(deviceOrigin)
        }
        deviceExportType
    }

    static void addManualInputMeasurementInfo(ManualInputOrigin origin,
                                              LaboratoryReportExtendedType laboratoryReportExtendedType) {
        ManualInputOrigin manualInputOrigin = (ManualInputOrigin) origin
        String enteredBy = manualInputOrigin.enteredBy
        if (enteredBy == CLINICIAN) {
            laboratoryReportExtendedType.measurementTransferredBy = MeasurementTransferredByType.TYPEDBYHCPROF
        } else {
            laboratoryReportExtendedType.measurementTransferredBy = MeasurementTransferredByType.TYPED
        }
    }

    static void addDefaultMeasurementInfo(LaboratoryReportExtendedType laboratoryReportExtendedType) {
        laboratoryReportExtendedType.measurementTransferredBy = MeasurementTransferredByType.TYPED
    }

    static void fillMeasurementTypeNameToClosureMap() {
        measurementTypeNameToExportTypesMap[MeasurementTypeName.BLOOD_PRESSURE.name()] = [
                new BloodPressureDiastolicExportType(), new BloodPressureSystolicExportType()
        ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.BLOODSUGAR.name()] = [ new BloodSugarExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.CRP.name()] = [ new CrpExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.LUNG_FUNCTION.name()] = [
                // TODO: Don't export the first two types, as they cannot currently be checked in the portal by a clinician
                // new KihLungFunctionFev1Fev6RatioExportType(),
                // new KihLungFunctionFev6ExportType(),
                new LungFunctionFev1ExportType()
        ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.PULSE.name()] = [ new PulseExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.SATURATION.name()] = [ new SaturationExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.TEMPERATURE.name()] = [ new TemperatureExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.URINE_BLOOD.name()] = [ new UrineBloodExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.URINE_GLUCOSE.name()] = [ new UrineGlucoseExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.URINE_LEUKOCYTES.name()] = [ new UrineLeukocytesExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.URINE_NITRITE.name()] = [ new UrineNitriteExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.URINE.name()] = [ new UrineProteinExportType() ]
        measurementTypeNameToExportTypesMap[MeasurementTypeName.WEIGHT.name()] = [ new WeightExportType() ]
    }

    private static def getDateAsXml(Date d) {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(d.getTime());
        DatatypeFactory df = DatatypeFactory.newInstance()
        return df.newXMLGregorianCalendar(gc)
    }

}