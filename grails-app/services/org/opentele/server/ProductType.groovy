package org.opentele.server

enum ProductType {

    LungMonitor("Lung Monitor"),
    PulseOximeter("Pulse Oximeter"),
    BloodPressureMonitor("Blood Pressure Monitor"),
    UrineAnalyzer("Urine Analyzer"),
    Weight("Weight"),
    Thermometer("Thermometer")

    private final String value

    ProductType(String value) {
        this.value = value
    }

    String value() {
        value
    }

}