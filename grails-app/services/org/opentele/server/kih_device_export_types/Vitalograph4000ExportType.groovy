package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class Vitalograph4000ExportType implements DeviceExportType {

    String medComId = "MCI00014"
    ProductType productType = ProductType.LungMonitor
    String manufacturer = "Vitalograph"
    String model = "4000 Lung Monitor Bluetooth"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer)
    }
}
