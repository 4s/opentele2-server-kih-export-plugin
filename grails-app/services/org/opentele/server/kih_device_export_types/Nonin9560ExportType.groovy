package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class Nonin9560ExportType implements DeviceExportType {

    String medComId = "MCI00005"
    ProductType productType = ProductType.PulseOximeter
    String manufacturer = "Nonin"
    String model = "Onyx II 9560 Bluetooth Pulse Oximeter"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer) &&
                deviceOrigin.model.contains('9560')
    }
}
