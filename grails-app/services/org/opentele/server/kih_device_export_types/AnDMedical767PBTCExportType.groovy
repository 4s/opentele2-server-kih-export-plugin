package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class AnDMedical767PBTCExportType implements DeviceExportType {

    String medComId = "MCI00004"
    ProductType productType = ProductType.BloodPressureMonitor
    String manufacturer = "A&D Medical"
    String model = "UA-767PlusBT-C Bluetooth"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer) &&
                deviceOrigin.model.contains('767') &&
                deviceOrigin.model.toLowerCase().contains("c")
    }
}
