package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

interface DeviceExportType {

    String getMedComId()
    ProductType getProductType()
    String getManufacturer()
    String getModel()
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin)

}
