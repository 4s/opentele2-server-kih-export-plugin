package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class AnDMedical302PBTExportType implements DeviceExportType {

    String medComId = "MCI00010"
    ProductType productType = ProductType.Thermometer
    String manufacturer = "A&D Medical"
    String model = "UT-302PlusBT Bluetooth"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer) &&
                deviceOrigin.model.contains('302')
    }
}
