package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class AnDMedical321PBTCExportType implements DeviceExportType {

    String medComId = "MCI00002"
    ProductType productType = ProductType.Weight
    String manufacturer = "A&D Medical"
    String model = "UC-321PlusBT-C Bluetooth"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer) &&
                deviceOrigin.model.contains('321')
    }
}
