package org.opentele.server.kih_device_export_types

import org.opentele.server.model.DeviceOrigin
import org.opentele.server.ProductType

class Nonin3230ExportType implements DeviceExportType {

    String medComId = "MCI00013"
    ProductType productType = ProductType.PulseOximeter
    String manufacturer = "Nonin"
    String model = "3230 Bluetooth Smart Pulse Oximeter"

    @Override
    String getMedComId() {
        medComId
    }

    @Override
    ProductType getProductType() {
        productType
    }

    @Override
    String getManufacturer() {
        manufacturer
    }

    @Override
    String getModel() {
        model
    }

    @Override
    Boolean checkIfSameDevice(DeviceOrigin deviceOrigin) {
        deviceOrigin.manufacturer.equalsIgnoreCase(manufacturer) &&
                deviceOrigin.model.contains('3230')
    }
}
