package org.opentele.server

enum NpuCode {

    BLOOD_PRESSURE_SYSTOLIC("DNK05472"),
    BLOOD_PRESSURE_DIASTOLIC("DNK05473"),
    CRP ("NPU19748"),
    BLOOD_SUGAR("NPU22089"),
    SATURATION("NPU03011"),
    FEV1("MCS88015"),
    FEV6("MCS88100"),
    FEV1_FEV6_RATIO("MCS88099"),
    PULSE("NPU21692"),
    TEMPERATURE("NPU08676"),
    URINE_GLUCOSE("NPU04207"),
    URINE_PROTEIN("NPU04206"),
    URINE_NITRITE("NPU21578"),
    URINE_LEUKOCYTES("NPU03987"),
    URINE_ERYTHROCYTES("NPU03963"),
    WEIGHT("NPU03804");

    private final String value

    NpuCode(String value) {
        this.value = value
    }

    String value() {
        value
    }

}