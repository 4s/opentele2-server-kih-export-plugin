package org.opentele.server

import dk.sosi.seal.model.Request
import org.opentele.server.model.Measurement

class KihDbExportService {

    def kihDbWebService
    def sosiService

    def exportToKihDatabase(String kihUrl, String createdByText) {

        Request request = null
        try {
            request = sosiService.createRequest()
        } catch (Exception exception) {
            log.error("Error occurred trying to create SosiRequest", exception)
            return
        }

        def measurementsToExport = getMeasurementsToExport()
        if (!measurementsToExport) {
            log.info("No measurements to export")
            return
        }

        measurementsToExport.each { Measurement measurement ->
            tryToExportMeasurement(measurement, request, kihUrl, createdByText)
        }
    }

    def getMeasurementsToExport() {
        Measurement.createCriteria().list {
            and {
                eq('exportedToKih', false)
                measurementType {
                    not {
                        inList('name', Measurement.notToBeExportedMeasurementTypes)
                    }
                }
            }
        }
    }

    def tryToExportMeasurement(Measurement measurement, Request request, String kihUrl, String createdByText) {

        setMeasurementExported(measurement, true)

        try {
            log.info("Exporting measurement (id:'${measurement.id}', type: ${measurement?.measurementType?.name}) to KIH DB")
            kihDbWebService.sendMeasurement(measurement, request, kihUrl, createdByText)
            log.info("Measurement (id:'${measurement.id}') was exported to KIH DB")
        } catch (Exception exception) {
            log.error("Error occurred while trying to export measurement (id:'${measurement.id}'):", exception)
            setMeasurementExported(measurement, false)
        }
    }

    private static setMeasurementExported(Measurement measurement, boolean flag) {
        measurement.exportedToKih = flag
        measurement.save(failOnError: true)
    }

}
