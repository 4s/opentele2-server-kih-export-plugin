package org.opentele.server

import org.springframework.beans.factory.annotation.Value

class KihExportJob {

    @Value('${kihExport.service.url:}')
    private String kihDbURL

    @Value('${kihExport.createdByText:}')
    private String createdByText

    def kihDbExportService

    def concurrent = false

    static triggers = {
        cron(name: 'DailyKihExportJob', cronExpression: "0 0/3 * * * ?") // Every 3 minutes
    }

    def execute() {
        if (kihDbURL) {
            log.debug("Exporting measurements to KIH Database")
            kihDbExportService.exportToKihDatabase(kihDbURL, createdByText)
        }
    }

}