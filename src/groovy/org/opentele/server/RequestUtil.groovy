package org.opentele.server

import dk.sosi.seal.model.Request
import dk.sosi.seal.xml.XmlUtil
import oio.medcom.monitoringdataset._1_0.CreateMonitoringDatasetRequestMessage
import org.w3c.dom.Document

import javax.xml.bind.JAXBContext
import javax.xml.bind.Marshaller
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory

class RequestUtil {

    static String generateRequest(CreateMonitoringDatasetRequestMessage requestMessage, Request request) {
        Marshaller marshaller = createMarshaller()
        Document doc = createDocument()
        marshaller.marshal(requestMessage, doc)
        request.body = doc.getDocumentElement()
        return XmlUtil.node2String(request.serialize2DOMDocument(), false, true)
    }

    private static Marshaller createMarshaller() {
        JAXBContext context = JAXBContext.newInstance(CreateMonitoringDatasetRequestMessage.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller
    }

    private static Document createDocument() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();
        doc
    }
}
